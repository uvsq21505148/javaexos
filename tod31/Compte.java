
public class Compte {

	public static void main(String[] args){
		System.out.println("Hezllo");
	}
	private int solde;
	
	//Compte jamais a decouvert
	//Sommes positives en paramètres
	//Debit ou virement que si assez de solde
	public Compte(int soldeInitial) {
		if(soldeInitial > 0) {
			solde = soldeInitial;
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	
	public int getSolde() { return solde; }
	public boolean credit(int montant) {
		if(montant < 0) {
			throw new IllegalArgumentException();
		}
		else {
			solde += montant;
			return true;
		}
	}
	
	public boolean debit(int montant) {
		if(montant < 0) {
			throw new IllegalArgumentException();
		}
		else if(solde < montant) {
			return false;
		}
		else {
			solde -= montant;
			return true;
		}
	}
	
	public boolean virement(Compte cible, int montant) {
		if(montant < 0) {
			throw new IllegalArgumentException();
		}
		else if(solde < montant) {
			return false;
		}
		else {
			this.debit(montant);
			cible.credit(montant);
			return true;
		}
	}
}
