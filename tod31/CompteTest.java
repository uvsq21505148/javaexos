import static org.junit.Assert.*;

import org.junit.Test;

public class CompteTest {

	@Test(expected = IllegalArgumentException.class)
    public void testCompteException() {
		Compte exception = new Compte(-15);
    }
    
    @Test
    public void testgetSolde() {
		Compte c = new Compte(100);
		assertTrue(c.getSolde() == 100);
    }
    
    @Test
    public void testCredit() {
		Compte c = new Compte(100);
		int montant = 50;
		
		assertTrue(c.credit(50));
		assertTrue(c.getSolde() == 150);
    }
    
    @Test
    public void testDebit() {
		Compte c1 = new Compte(100);
		int montant = 50;
		
		assertTrue(c1.debit(montant));
		assertEquals(c1.getSolde(), 50);
		
		Compte c2 = new Compte(50);
		montant = 100;
		
		assertFalse(c2.debit(montant));
		assertEquals(c2.getSolde(), 50);
    }
    
    @Test
    public void testVirement() {
		Compte c1 = new Compte(100);
		Compte c2 = new Compte(50);
		int montant = 75;
		
		assertTrue(c1.virement(c2, 75));
		assertTrue(c1.getSolde() == 25);
		assertTrue(c2.getSolde() == 125);
		
		Compte c3 = new Compte(100);
		Compte c4 = new Compte(50);
		montant = 75;
		
		assertFalse(c4.virement(c3, 75));
		assertTrue(c4.getSolde() == 50);
		assertTrue(c3.getSolde() == 100);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreditException() {
		Compte c1 = new Compte(100);
		int montant = -50;
		
		c1.credit(montant);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDebitException() {
		Compte c1 = new Compte(100);
		int montant = -50;
		
		c1.debit(montant);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDebitVirementException() {
		Compte c1 = new Compte(100);
		Compte c2 = new Compte(100);
		int montant = -50;
		
		c1.virement(c2, montant);
    }

}
