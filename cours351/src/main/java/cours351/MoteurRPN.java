package cours351;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class MoteurRPN {
	
	private LinkedList<Double> operandes;
	
	public static double MAX_VALUE = 100;
	public static double MIN_VALUE = 0.01;
	
	public MoteurRPN() {
		operandes = new LinkedList<Double>();
	}
	
	public MoteurRPN(LinkedList<Double> liste) {
		operandes = new LinkedList<Double>(liste);
	}
	
	public void save(Double operande) {
		
		if(Math.abs(operande) > MAX_VALUE)
			throw new InfinityException(operande);
		else if(Math.abs(operande) < MIN_VALUE)
			operandes.push(0.0);
		else
			operandes.push(operande);
	}
	
	public void apply(Operation operation) 
		throws PasAssezDOperandesException, InfinityException {
			
		if(operandes.size() < 2) {
			throw new PasAssezDOperandesException(Integer.valueOf(operandes.size()).toString());
		}
		else {
			double tmp = operation.eval(operandes.pop(), operandes.pop());
			if(Math.abs(tmp) > MAX_VALUE)
				throw new InfinityException(tmp);
			else if(Math.abs(tmp) < MIN_VALUE)
				operandes.push(0.0);
			else
				operandes.push(tmp);
		}
	}
	
	public LinkedList<Double> getOperandes() {
		return operandes;
	}
}
