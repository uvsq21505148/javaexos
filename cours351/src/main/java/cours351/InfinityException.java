package cours351;

public class InfinityException extends ArithmeticException {
	
	public InfinityException(double inifinity) {
			super((Math.signum(inifinity) == 1) ? "Overflow" : "Underflow");
	}
}
