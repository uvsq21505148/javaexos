package cours351;

public enum Operation {
	PLUS ('+'){
		public double eval(double a, double b){return a + b;}
	},
	MOINS ('-'){
		public double eval(double a, double b){return a - b;}
	},
	MULT ('*'){
		public double eval(double a, double b){return a * b;}
	},
	DIV ('/'){
		public double eval(double a, double b){return a / b;}
	};
	
	private char symbole;
	
	Operation(char symbole) {
		this.symbole = symbole;
	}
	
	public abstract double eval(double a, double b);
}
