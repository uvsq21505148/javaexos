package cours351;

import java.util.Scanner;
import java.util.NoSuchElementException;

public class CalculatriceRPN {
	
	public static void main(String args[]) {
		SaisieRPN saisie = new SaisieRPN();
		MoteurRPN moteur = new MoteurRPN();
		Scanner sc = new Scanner(System.in);
		
		String commande;
		boolean noExit;
		
		//Lancement de la calculatrice
		do {
			try {
				commande = sc.next(); // Ne prend pas en compte les commandes vides, supprime les espaces
				noExit = saisie.execute(commande, moteur);
				
				System.out.println("Pile : " + moteur.getOperandes().toString());
			}
			catch(PasUneCommandeException e1) {
				System.out.println(e1.getMessage());
				System.out.println("Pile : " + moteur.getOperandes().toString());
				noExit = true;
			}
			catch(PasAssezDOperandesException e2) {
				System.out.println(e2.getMessage());
				System.out.println("Pile : " + moteur.getOperandes().toString());
				noExit = true;
			}
			catch(InfinityException e3) {
				System.out.println(e3.getMessage());
				noExit = false;
			}
		} while(noExit);
		
		//Fin du programme
		System.out.println("La calculatrice va s'éteindre ...");
		try {
			Thread.sleep(1000); // 1 seconde
		} catch(InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
}
