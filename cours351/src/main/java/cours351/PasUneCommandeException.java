package cours351;

public class PasUneCommandeException extends IllegalArgumentException {
	
	public PasUneCommandeException() {
		super("Veuillez entrer un nombre, une opérande, ou exit pour quitter.");
	}
}
