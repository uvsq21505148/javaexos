package cours351;

public class SaisieRPN{
		
	public SaisieRPN() {}
	
	/**
	 * @return true si on souhaite poursuivre la saisie, false sinon.
	 * */
	public boolean execute(String commande, MoteurRPN moteur)
		throws PasUneCommandeException, InfinityException, PasAssezDOperandesException {
		
		try {
			switch(commande) {	
				case "exit" :
					return false;
				case "+" :
					moteur.apply(Operation.PLUS);
					return true;
				case "-" :
					moteur.apply(Operation.MOINS);
					return true;
				case "*" :
					moteur.apply(Operation.MULT);
					return true;
				case "/" :
					moteur.apply(Operation.DIV);
					return true;
				default :
					moteur.save(Double.parseDouble(commande)); //throw NumberFormatExc qui herite de IllegalArgumentExc
					return true;
			}
		}
		catch(PasAssezDOperandesException | InfinityException e1) {
			throw e1;
		}
		catch(IllegalArgumentException e2) { // Ni une commande, ni un double "parseable", attrape le NumberFormatException
			throw new PasUneCommandeException();
		}
	}	
}
