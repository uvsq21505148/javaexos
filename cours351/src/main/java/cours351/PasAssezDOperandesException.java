package cours351;

import java.util.NoSuchElementException;

public class PasAssezDOperandesException extends NoSuchElementException {
	
	public PasAssezDOperandesException(String message) {
		super("Pas assez d'opérandes : besoin de 2, " + message + " disponible");
	}
}
