package cours351;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.LinkedList;
import java.util.Arrays;

public class TestMoteurRPN {
		
	@Test
	public void testSave(){
		MoteurRPN moteur1 = new MoteurRPN();
		moteur1.save(1.0);
		assertTrue(moteur1.getOperandes().equals(new LinkedList<Double>(Arrays.asList(1.0))));
		
		//Une valeur plus petite en valeur absolue du minimum identifiable est considérée égale à 0
		MoteurRPN moteur2 = new MoteurRPN();
		moteur2.save(MoteurRPN.MIN_VALUE * (1 - MoteurRPN.MIN_VALUE));
		assertTrue(moteur2.getOperandes().equals(new LinkedList<Double>(Arrays.asList(0.0))));
	}
	
	//La plus petite valeur superieure au MAX n'est pas supportée
	@Test(expected = InfinityException.class)
	public void testSaveException(){
		MoteurRPN moteur = new MoteurRPN();
		moteur.save(MoteurRPN.MAX_VALUE + MoteurRPN.MIN_VALUE);
	}
	
	@Test
	public void testApply(){
		MoteurRPN moteur1 = new MoteurRPN(new LinkedList<Double>(Arrays.asList(-4.6542251, 38.546546)));
		moteur1.apply(Operation.PLUS);
		assertTrue(moteur1.getOperandes().equals(new LinkedList<Double>(Arrays.asList(-4.6542251 + 38.546546))));
		
		MoteurRPN moteur2 = new MoteurRPN(new LinkedList<Double>(Arrays.asList(-4.6542251, 38.546546)));
		moteur2.apply(Operation.MOINS);
		assertTrue(moteur2.getOperandes().equals(new LinkedList<Double>(Arrays.asList(-4.6542251 - 38.546546))));
		
		MoteurRPN moteur3 = new MoteurRPN(new LinkedList<Double>(Arrays.asList(MoteurRPN.MIN_VALUE, 1 - MoteurRPN.MIN_VALUE)));
		moteur3.apply(Operation.MULT);
		assertTrue(moteur3.getOperandes().equals(new LinkedList<Double>(Arrays.asList(0.0))));
		
		MoteurRPN moteur4 = new MoteurRPN(new LinkedList<Double>(Arrays.asList(5.0, 3.0)));
		moteur4.apply(Operation.DIV);
		assertTrue(moteur4.getOperandes().equals(new LinkedList<Double>(Arrays.asList(5.0 / 3.0))));
	}
	
	//Le résultat d'ue opération ne doit pas dépasser le MAX supporté
	@Test(expected = InfinityException.class)
	public void testApplyInfinityException1(){
		MoteurRPN moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(MoteurRPN.MAX_VALUE, MoteurRPN.MIN_VALUE)));
		moteur.apply(Operation.PLUS);
	}
	
	public void testApplyInfinityException2(){
		MoteurRPN moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(MoteurRPN.MAX_VALUE, 1 + MoteurRPN.MIN_VALUE)));
		moteur.apply(Operation.MULT);
	}
	
	//Le lancement de l'exception est indépendant de l'opération utilisée
	@Test(expected = PasAssezDOperandesException.class)
	public void testApplyPasAssezDOperandesException1(){
		MoteurRPN moteur = new MoteurRPN();
		moteur.apply(Operation.PLUS);
	}
	
	//Le lancement de l'exception est indépendant de l'opération utilisée
	@Test(expected = PasAssezDOperandesException.class)
	public void testApplyPasAssezDOperandesException2(){
		MoteurRPN moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(0.0)));
		moteur.apply(Operation.PLUS);
	}
}
