package cours351;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.LinkedList;
import java.util.Arrays;

public class TestSaisieRPN {
	
	//retour false si on demande à quitter le programme, vrai si la commande est réussie
	@Test
	public void testExecute(){
		SaisieRPN saisie = new SaisieRPN();
		MoteurRPN moteur = new MoteurRPN();
		assertFalse(saisie.execute("exit", moteur));
		assertTrue(saisie.execute("0.0", moteur));
		assertTrue(saisie.execute("13.16001", moteur));
		assertTrue(saisie.execute("-13.16001", moteur));
		assertTrue(saisie.execute("1.04e1", moteur));
		
		moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(1.0, 2.0)));
		assertTrue(saisie.execute("+", moteur));
		moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(1.0, 2.0)));
		assertTrue(saisie.execute("-", moteur));
		moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(1.0, 2.0)));
		assertTrue(saisie.execute("*", moteur));
		moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(1.0, 2.0)));
		assertTrue(saisie.execute("/", moteur));
	}
	
	//La commande est un nombre supérieur au MAX
	@Test(expected = InfinityException.class)
	public void testExecuteInfinityException1(){
		SaisieRPN saisie = new SaisieRPN();
		MoteurRPN moteur = new MoteurRPN();
		saisie.execute(Double.valueOf(MoteurRPN.MAX_VALUE + MoteurRPN.MIN_VALUE).toString(), moteur);
	}
	
	//Le résultat de la commande est un nombre supérieur au MAX
	@Test(expected = InfinityException.class)
	public void testExecuteInfinityException2(){
		SaisieRPN saisie = new SaisieRPN();
		MoteurRPN moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(MoteurRPN.MAX_VALUE, MoteurRPN.MIN_VALUE)));
		saisie.execute("+", moteur);
	}
	
	//Opération exécutée sur une pile d'unélément
	@Test(expected = PasAssezDOperandesException.class)
	public void testExecuteException3(){
		SaisieRPN saisie = new SaisieRPN();
		MoteurRPN moteur = new MoteurRPN(new LinkedList<Double>(Arrays.asList(1.0)));
		saisie.execute("+", moteur);
	}
	
	//Opération exécutée sur une pile vide
	@Test(expected = PasAssezDOperandesException.class)
	public void testExecuteException1(){
		SaisieRPN saisie = new SaisieRPN();
		MoteurRPN moteur = new MoteurRPN();
		saisie.execute("+", moteur);
	}
	
	//La commande n'est ni une opération, ni un nombre flottant
	@Test(expected = PasUneCommandeException.class)
	public void testExecutePasUneCommandeException(){
		SaisieRPN saisie = new SaisieRPN();
		MoteurRPN moteur = new MoteurRPN();
		saisie.execute("aelzdhi", moteur);
	}
}
