
/**
 * Décrivez votre classe ChaineCryptee ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class ChaineCryptee
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private String crypte;
    private int decalage;
    
    public static void main(String[] args) {
		ChaineCryptee c1 = FromDecrypted("hello", 3);
		ChaineCryptee c2 = FromCrypted(c1.crypte(), 3);
		
		System.out.println("Test c1.crypte() : " + c1.crypte() + " = c2.crypte() : " + c2.crypte());
		System.out.println("Test c1.decrypte() : " + c1.decrypte() + " = c2.decrypte() : " + c2.decrypte());
	}

    /**
     * Constructeur d'objets de classe ChaineCryptee
     */
    private ChaineCryptee(String clair, int decalage)
    {
        // initialisation des variables d'instance
        StringBuilder str = new StringBuilder();
        int i;
        
        if(clair != null) {
            for(i = 0; i < clair.length(); i++) {
                str.append(decaleCaractere(clair.toUpperCase().charAt(i), decalage));
            }
        }
        this.crypte = str.toString();
        this.decalage = decalage;
    }
    
    public static ChaineCryptee FromCrypted(String crypte, int decalage) { 
        
        StringBuilder str = new StringBuilder();
        int i;
        
        if(crypte != null) {
            for(i = 0; i < crypte.length(); i++) {
                str.append(decaleCaractere(crypte.toUpperCase().charAt(i), - decalage));
            }
        }
        ChaineCryptee ch = new ChaineCryptee(str.toString(), decalage);
        return ch;
    }
    
    public static ChaineCryptee FromDecrypted(String clair, int decalage) {
        ChaineCryptee ch = new ChaineCryptee(clair, decalage);
        return ch;
    }

    /**
     * @return     la chaine en clair, null si chaine de caractere initialisée à null
     */
    public String decrypte()
    {
        StringBuilder str = new StringBuilder();
        int i;
        
        for(i = 0; i < crypte.length(); i++) {
            str.append(decaleCaractere(crypte.toUpperCase().charAt(i), - decalage));
        }
        return str.toString();
    }
    
    
    private static char decaleCaractere(char c, int decalage) {
            return (c < 'A' || c > 'Z' ) ? c : (char) ((( c - 'A' + decalage) % 26) + 'A');
    }
    
    /**
     * @return      retourne la chaine cryptée, ou la chaine vide si la chaine en clair est "null"
     */
    public String crypte()
    {
        // Insérez votre code ici
        return crypte;
    }
}
