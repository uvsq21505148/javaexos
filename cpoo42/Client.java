
/**
 * Décrivez votre classe Client ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Client
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private String nom;
    private Serveur serveur;

    /**
     * Constructeur d'objets de classe Client
     */
    public Client(String nom)
    {
        this.nom = nom;
        this.serveur = null;
    }

    public boolean seConnecter(Serveur serveur)
    {
        boolean reponse = serveur.connecter(this);
        if(reponse == false) //On ne peut pas se connecter
            return false;
        else {
            this.serveur = serveur;
            return true;
        }
    }
    
    public void envoyer(String message)
    {
        if(serveur != null)
            serveur.diffuser(nom + ": " + message);
    }
    
    public String recevoir(String message)
    {
        System.out.println(message);
        return message;
    }
}
