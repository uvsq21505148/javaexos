import java.util.ArrayList;
import java.util.Iterator;
/**
 * D�crivez votre classe Serveur ici.
 *
 * @author (votre nom)
 * @version (un numero de version ou une date)
 */
public class Serveur
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    private ArrayList<Client> clients;
	
	public static void main(String[] args) {
		Serveur s = new Serveur();
		Client c1 = new Client("c1");
		Client c2 = new Client("c2");
		
		c1.seConnecter(s);
		c2.seConnecter(s);
		
		Client c3 = new Client("c3");
		c3.seConnecter(s);
		c3.envoyer("Hello world!");
	}
	
    /**
     * Constructeur d'objets de classe Serveur
     */
    public Serveur()
    {
        clients = new ArrayList<Client>();
    }

    public boolean connecter(Client client)
    {
        if(clients.contains(client)) {
            return false;
        }
        else {
            return clients.add(client);
        }
    }
    
    public void diffuser(String message)
    {
        Iterator<Client> liste_clients = clients.iterator();
        
        while(liste_clients.hasNext()) {
            liste_clients.next().recevoir(message);
        }
    }
}
