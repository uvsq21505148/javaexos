import java.util.ArrayList;
import java.util.Iterator;

/**
 * Write a description of class Repertoire here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Repertoire extends Composant
{
    // instance variables - replace the example below with your own
    private ArrayList<Composant> composants;
    
    public static void main(String[] args) {
		Fichier f1 = new Fichier("f1", 10);
		Fichier f2 = new Fichier("f1", 20);
		
		Repertoire r1 = new Repertoire("r1");
		Repertoire r2 = new Repertoire("r2");
		
		r1.ajouter(f1);
		System.out.println("Taille r1, 1 fichier taille 10 : " + r1.calculTaille());
		r2.ajouter(f2);
		r2.ajouter(r1);
		System.out.println("Taille r2, 1 fichier taille 10, et r1 : " + r2.calculTaille());
		System.out.println("Tentative ajout de r2 dans r1 : " + r1.ajouter(r2));
		//faux car r2 contient r1
	}

    /**
     * Constructor for objects of class Repertoire
     */
    public Repertoire(String nom)
    {
        this.setNom(nom);
        this.composants = new ArrayList<Composant>();
    }
    
    public boolean contient(Composant comp) {
        Iterator<Composant> liste_comp = composants.iterator();
        Composant tmp;
        
        while(liste_comp.hasNext()) {
            tmp = liste_comp.next();
            
            if(tmp.equals(comp)){
                return true;
            }
            else if(tmp instanceof Repertoire) {
                //On teste récursivement si comp est dans un sous-repertoire
                 if(((Repertoire) tmp).contient(comp)) {
                    return true;
                }
            }
        }
        //On a parcouru dans tout le répertoire
        return false;
    }
    
    public boolean ajouter(Composant comp)
    {
        if(this.contient(comp)) {
            return false;
        }
        else if((comp instanceof Repertoire) && (((Repertoire) comp).contient(this))) {
            return false;
        }
        else {
            return composants.add(comp);
        }
    }
    
    public boolean retirer(Composant comp)
    {
        return composants.remove(comp);
    }
    
    public int calculTaille()
    {
        Iterator<Composant> liste_comp = composants.iterator();
        int somme = 0;
        
        while(liste_comp.hasNext()) {
            somme += liste_comp.next().calculTaille(); 
        }
        return somme;
    }
}
