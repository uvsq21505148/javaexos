
/**
 * Décrivez votre classe Fraction ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Fraction implements Comparable<Fraction>
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private final int num;
    private final int denum;
    
    public static Fraction ZERO = new Fraction();
    public static Fraction UN = new Fraction(1, 1);
    
    public static void main(String[] args) {
		Fraction f1 = Fraction.getReducedFraction(2, 4);
		Fraction f2 = Fraction.getReducedFraction(1, 3);
		Fraction f3 = Fraction.getReducedFraction(1, 2);
		Fraction f4 = Fraction.getReducedFraction(7, 5);
		
		// doubleValue add equals compareTo
		System.out.println("f1 = 1 / 2 doubleValue: " + f1.doubleValue());
		System.out.println("f1 + f2 = 1/2 + 1/3 : " + f1.add(f2).toString());
		System.out.println("f1 = f2 : " + f1.equals(f2));
		System.out.println("f1 < f4 : " + f1.compareTo(f4));
		System.out.println("f1 > f2 : " + f1.compareTo(f2));
	}

    public Fraction(int num, int denum)
    {
        this.num = num;
        if(denum == 0) {
            throw new ArithmeticException();
        }
        this.denum = denum;
    }
    
    public Fraction(int num)
    {
        this(num, 1);
    }
    
    public Fraction()
    {
        this(0, 1);
    }
    
    public static int pgcd(int a,int b) {
        int r = a;
        //dernier reste non nul
        while (r != 0) {
            r = a%b;
            a=b;
            b=r;
        }
        return a;
    }
    
    public static Fraction getReducedFraction(int num, int denum)
    {
        int pgcd = pgcd(num, denum);
        return new Fraction(num / pgcd, denum / pgcd);
    }
    
    public int getNum(){ return num; }
    public int getDenum(){ return denum; }
    public double doubleValue(){ return ((double) num / (double) denum); }
    
    public Fraction add(Fraction f)
    {
        return getReducedFraction(num * f.denum + denum * f.num, denum * f.getDenum());
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(o instanceof Fraction) {
            Fraction f1 = getReducedFraction(num, denum);
            Fraction f2 = getReducedFraction(((Fraction) o).getNum(), ((Fraction) o).getDenum());
            
            return f1.getNum() == f2.getNum() && f1.getDenum() == f2.getDenum();
        }
        else {
            return false;
        }
    }
    
    @Override
    public String toString()
    {
        if(denum != 1) {
            return Integer.valueOf(num).toString() + " / " + Integer.valueOf(denum).toString();
        }
        else {
            return Integer.valueOf(num).toString();
        }
    }
    
    /**
     * Comparer deux nombres revient à comparer le signe de leur différence
     * 
     * @return -1 if this is less than object, +1 if this is greater than object, 0 if they are equal.
     */
    public int compareTo(Fraction o)
    {
        //a - b = a + (-b)
        Fraction f1 = getReducedFraction(- o.getNum(), o.getDenum()); // b = - b
        Fraction f2 = this.add(f1); // a - b
        
        if(f2.getNum() == 0) return 0;
        return Math.abs(f2.getNum() * f2.getDenum()) / (f2.getNum() * f2.getDenum());
          
    }
}
