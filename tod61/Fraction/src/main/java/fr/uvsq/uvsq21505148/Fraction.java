package fr.uvsq.uvsq21505148;

public class Fraction implements Comparable<Fraction>
{
	public static void main(String[] args){}
	
    private final int num;
    private final int denum;
    
    public static Fraction ZERO = new Fraction();
    public static Fraction UN = new Fraction(1, 1);

    public Fraction(int num, int denum)
    {
		this.num = num;
        if(denum == 0) {
            throw new ArithmeticException();
        }
        this.denum = denum;
    }
    
    public Fraction(int num)
    {
		this(num, 1);
    }
    
    public Fraction()
    {
		this(0, 1);
    }
    
    public static Fraction getReducedFraction(int num, int denum)
    {
		return new Fraction();
    }
    
    public int getNum(){ return num; }
    public int getDenum(){ return denum; }
    
    public double doubleValue(){
		return ((double) num / (double) denum);
	}
    
    @Override
    public boolean equals(Object o)
    {
		return false;
    }
    
	@Override
    public String toString()
    {
        if(denum != 1) {
            return Integer.valueOf(num).toString() + " / " + Integer.valueOf(denum).toString();
        }
        else {
            return Integer.valueOf(num).toString();
        }
    }
    
    /**
     * Comparer deux nombres revient à comparer le signe de leur différence
     * 
     * @return -1 if this is less than object, +1 if this is greater than object, 0 if they are equal.
     */
    public int compareTo(Fraction o)
    {
		return 0;
    }
}
