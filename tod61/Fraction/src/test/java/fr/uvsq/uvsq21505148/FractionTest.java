package fr.uvsq.uvsq21505148;

import org.junit.*;
import static org.junit.Assert.*;

public class FractionTest
{
	
	@Test(expected = ArithmeticException.class)
	public void testFraction()
	{
		new Fraction(1, 0);
	}
	
	@Test
    public void testgetNum()
    {
		assertTrue((new Fraction(2, 3)).getNum()== 2);
		assertTrue((new Fraction(2)).getNum()	== 2);
		assertTrue((new Fraction()).getNum()	== 0);
	}
	
    @Test
    public void testgetDenum()
    {
		assertTrue((new Fraction(2, 3)).getDenum()	== 3);
		assertTrue((new Fraction(2)).getDenum()		== 1);
		assertTrue((new Fraction()).getDenum()		== 1);
	}
	
	@Test
	public void testZeroUn()
	{
		assertTrue(Fraction.ZERO.getNum()	== 0);
		assertTrue(Fraction.ZERO.getDenum()	== 1);
		assertTrue(Fraction.UN.getNum()		== 1);
		assertTrue(Fraction.UN.getDenum()	== 1);
	}
    
    //~ @Test
    //~ public void testgetReducedFraction()
    //~ {
		//~ Fraction irreductible = new Fraction(2, 7);
		//~ Fraction paire1 = new Fraction(1, 2);
		//~ Fraction paire2 = new Fraction(2, 4);
		
		//~ assertTrue(Fraction.getReducedFraction(irreductible).getNum() 	== 2);
		//~ assertTrue(Fraction.getReducedFraction(irreductible).getDenum() == 7);
		
		//~ assertTrue(Fraction.getReducedFraction(paire1).getNum()		== Fraction.getReducedFraction(paire2).getNum());
		//~ assertTrue(Fraction.getReducedFraction(paire1).getDenum()	== Fraction.getReducedFraction(paire2).getDenum());
    //~ }
	
	@Test
    public void testdoubleValue()
    {
		Fraction entier		= new Fraction(2);
		Fraction finie		= new Fraction(1, 2);
		Fraction infinie	= new Fraction(1, 3);
		
		assertTrue(entier.doubleValue()	== 2);
		assertTrue(finie.doubleValue()	== 0.5);
		assertTrue(infinie.doubleValue()== 1.0 / 3.0);
	}
    
    //~ @Test
    //~ public void testequals()
    //~ {
		//~ assertTrue((new Fraction(2)).equals(new Fraction(2, 1))		, true);
		//~ assertTrue((new Fraction()).equals(new Fraction(0, 1))		, true);
		//~ assertTrue((new Fraction(3, 2)).equals(new Fraction(3, 2))	, true);
		//~ assertTrue((new Fraction(1, 3)).equals(new Fraction(2, 6))	, true);
    //~ }
    
    @Test
    public void testtoString()
    {
		Fraction f1	= new Fraction(2);
		Fraction f2	= new Fraction(1, 2);
		Fraction f3	= new Fraction();
		
		assertTrue(f1.toString().compareTo("2")		== 0);
		assertTrue(f2.toString().compareTo("1 / 2")	== 0);
		assertTrue(f3.toString().compareTo("0")		== 0);
    }
    
    //~ @Test
    //~ public void testcompareTo()
    //~ {
		//~ Fraction f1	= new Fraction(2);
		//~ Fraction f2	= new Fraction(1, 2);
		//~ Fraction f3	= new Fraction(1, 3);
		
		//~ assertTrue(f1.compareTo(f1)		== 0); // f1 = f1
		//~ assertTrue(f1.compareTo(f2)		== 1); // f1 > f2
		//~ assertTrue(f3.compareTo(f2)		== -1); // f3 < f2 
    //~ }
}	
